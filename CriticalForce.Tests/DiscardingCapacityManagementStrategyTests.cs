﻿using System.Linq;
using CriticalForce.CatalogPositionCapacityFilter;
using CriticalForce.DiContainer;
using CriticalForce.Logger;
using NUnit.Framework;
using SimpleInjector;

namespace CriticalForce.Tests
{
    /// <summary>
    /// Tests for [DiscardingCapacityManagementStrategy]
    /// Test naming convention:
    /// {method name}_{scenario}_{expected behaviour}
    /// </summary>
    [TestFixture]
    public class DiscardingCapacityManagementStrategyTests
    {
        private Container _container;
        private TestSetProvider _testSetProvider;

        [SetUp]
        public void Setup()
        {
            var containerProvider = new BasicConfigurationDiContainerProvider();
            _container = containerProvider.Provide();
            _testSetProvider = new TestSetProvider();
        }
        
        [Test]
        public void FilterProducts_PositionAmountsLessThanCapacity_PositionAmountsUnchanged()
        {
            // Arrange
            var positions = _testSetProvider.GetDiversePositions();
            var startingAmount = positions.Sum(position => position.Amount);
            var logger = _container.GetInstance<ILogger>();
            var capacityManagementStrategy = new DiscardingCatalogPositionCapacityFilter(logger);
            
            // Act
            var filteredPositions = capacityManagementStrategy.FilterProducts(positions, startingAmount);
            var filteredAmount = filteredPositions.Sum(position => position.Amount);

            // Assert
            Assert.AreEqual(startingAmount, filteredAmount);
        }
        
        [Test]
        public void FilterProducts_PositionAmountsGreaterThanCapacity_PositionAmountsReducedToCapacity()
        {
            // Arrange
            var positions = _testSetProvider.GetDiversePositions();
            var startingAmount = positions.Sum(position => position.Amount);
            var capacity = startingAmount - 1;
            var logger = _container.GetInstance<ILogger>();
            var capacityManagementStrategy = new DiscardingCatalogPositionCapacityFilter(logger);
            
            // Act
            var filteredPositions = capacityManagementStrategy.FilterProducts(positions, capacity);
            var filteredAmount = filteredPositions.Sum(position => position.Amount);

            // Assert
            Assert.AreEqual(capacity, filteredAmount);
        }
    }
}