﻿using System.Collections.Generic;
using CriticalForce.Catalog;
using CriticalForce.Item.Drink;
using CriticalForce.Item.Food;
using CriticalForce.Item.Weapon;

namespace CriticalForce.Tests
{
    /// <summary>
    /// Helper class providing prepared data sets for test arrangement
    /// </summary>
    public class TestSetProvider
    {
        // Provides [CatalogPosition]s with [IItem]s of all different types
        public List<CatalogPosition> GetDiversePositions()
        {
            var item1 = new Weapon("AK-47", 30, 30);
            var item2 = new Food("CritBurger", "The most explosive burger you'll ever try!");
            var item3 = new Drink("CritCola", 1);
            
            var position1 = new CatalogPosition(item1, 299.00, 1);
            var position2 = new CatalogPosition(item2, 10.50, 10);
            var position3 = new CatalogPosition(item3, 5.20, 15);
            var positions = new List<CatalogPosition>{position1, position2, position3};

            return positions;
        }
        
        // Provides [CatalogPosition]s with multiple different [Weapon]s
        public List<CatalogPosition> GetWeaponPositions()
        {
            var item1 = new Weapon("AK-47", 30, 30);
            var item2 = new Weapon("M-16", 24, 24);
            var item3 = new Weapon("Colt Python", 6, 6);
            
            var position1 = new CatalogPosition(item1, 250.00, 5);
            var position2 = new CatalogPosition(item2, 300.00, 5);
            var position3 = new CatalogPosition(item3, 159.99, 10);
            var positions = new List<CatalogPosition>{position1, position2, position3};

            return positions;
        }
    }
}