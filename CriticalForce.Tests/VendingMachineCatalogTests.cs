﻿using System.Collections.Generic;
using System.Linq;
using CriticalForce.Catalog;
using CriticalForce.Item.Drink;
using CriticalForce.Item.Food;
using CriticalForce.Item.Weapon;
using NUnit.Framework;

namespace CriticalForce.Tests
{
    /// <summary>
    /// Tests for [VendingMachineCatalog]
    /// Test naming convention:
    /// {method name}_{scenario}_{expected behaviour}
    /// </summary>
    [TestFixture]
    public class VendingMachineCatalogTests
    {
        private TestSetProvider _testSetProvider;

        [SetUp]
        public void Setup()
        {
            _testSetProvider = new TestSetProvider();
        }
        
        [Test]
        public void GetItemEntry_RequestedItemInCatalog_ReturnsItemPosition()
        {
            // Arrange
            var item = new Drink("CritCola", 1);
            var position1 = new CatalogPosition(item, 1.10, 1);
            var positions = new List<CatalogPosition>{position1};
            var catalog = new VendingMachineCatalog(positions);
            
            // Act
            var result = catalog.GetItemEntry(item);

            // Assert
            Assert.AreEqual(position1, result);
        }
        
        [Test]
        public void GetItemEntry_RequestedItemNotInCatalog_ReturnsNull()
        {
            // Arrange
            var item = new Drink("CritCola", 1);
            var catalog = new VendingMachineCatalog(new List<CatalogPosition>());
            
            // Act
            var result = catalog.GetItemEntry(item);

            // Assert
            Assert.IsNull(result);
        }
        
        [Test]
        public void GetTotalCapacity_MultiplePositionsInCatalog_ReturnsSumOfPositionAmounts()
        {
            // Arrange
            var positions = _testSetProvider.GetDiversePositions();
            var expectedAmount = positions.Sum(position => position.Amount);
            var catalog = new VendingMachineCatalog(positions);
            
            // Act
            var result = catalog.GetTotalCapacity();

            // Assert
            Assert.AreEqual(expectedAmount, result);
        }
        
        [Test]
        public void DecrementItemCount_PositionAmountGreaterThanZero_PositionAmountReducedByOne()
        {
            // Arrange
            var startingAmount = 1;
            var item = new Drink("CritCola", 5);
            var position1 = new CatalogPosition(item, 1.10, startingAmount);
            var positions = new List<CatalogPosition>{position1};
            var catalog = new VendingMachineCatalog(positions);
            
            // Act
            catalog.DecrementItemCount(item);

            // Assert
            Assert.AreEqual(startingAmount - 1, catalog.GetItemEntry(item).Amount);
        }
        
        [Test]
        public void DecrementItemCount_PositionAmountEqualToZero_PositionAmountUnchanged()
        {
            // Arrange
            var startingAmount = 0;
            var item = new Drink("CritCola", 5);
            var position1 = new CatalogPosition(item, 1.10, startingAmount);
            var positions = new List<CatalogPosition>{position1};
            var catalog = new VendingMachineCatalog(positions);
            
            // Act
            catalog.DecrementItemCount(item);

            // Assert
            Assert.AreEqual(startingAmount, catalog.GetItemEntry(item).Amount);
        }

        [Test]
        public void UpdatePositions_CatalogEmpty_PositionsEqual()
        {
            // Arrange
            var positions = _testSetProvider.GetDiversePositions();
            var catalog = new VendingMachineCatalog(new List<CatalogPosition>());
            
            // Act
            catalog.UpdatePositions(positions);

            // Assert
            foreach (var position in positions)
            {
                Assert.AreEqual(catalog.GetItemEntry(position.Item), position);
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Price, position.Price);
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Amount, position.Amount);
            }
        }
        
        [Test]
        public void UpdatePositions_AddSamePositions_PositionPricesAndAmountsChanged()
        {
            // Arrange
            var item1 = new Weapon("AK-47", 30, 30);
            var item2 = new Food("CritBurger", "The most explosive burger you'll ever try!");
            var item3 = new Drink("CritCola", 1);
            
            var position1 = new CatalogPosition(item1, 1.10, 1);
            var position2 = new CatalogPosition(item2, 2.20, 2);
            var position3 = new CatalogPosition(item3, 3.30, 3);
            var positions1 = new List<CatalogPosition>{position1, position2, position3};
            
            var position12 = new CatalogPosition(item1, 4.40, 4);
            var position22 = new CatalogPosition(item2, 5.50, 5);
            var position32 = new CatalogPosition(item3, 6.60, 6);
            var positions2 = new List<CatalogPosition>{position12, position22, position32};

            var catalog = new VendingMachineCatalog(positions1);
            
            // Act
            catalog.UpdatePositions(positions2);

            // Assert
            Assert.AreEqual(catalog.GetItemEntry(item1).Price, position12.Price);
            Assert.AreEqual(catalog.GetItemEntry(item1).Amount, position1.Amount + position12.Amount);
            Assert.AreEqual(catalog.GetItemEntry(item2).Price, position22.Price);
            Assert.AreEqual(catalog.GetItemEntry(item2).Amount, position2.Amount + position22.Amount);
            Assert.AreEqual(catalog.GetItemEntry(item3).Price, position32.Price);
            Assert.AreEqual(catalog.GetItemEntry(item3).Amount, position3.Amount + position32.Amount);
        }
        
        [Test]
        public void UpdatePositions_AddNewPositions_OldAndNewPositionsInCatalog()
        {
            // Arrange
            var positions = _testSetProvider.GetDiversePositions();
            var catalog = new VendingMachineCatalog(positions);
            var additionalPositions = _testSetProvider.GetWeaponPositions();
            
            // Act
            catalog.UpdatePositions(additionalPositions);

            // Assert
            foreach (var position in positions)
            {
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Item, position.Item);
            }
            foreach (var position in additionalPositions)
            {
                Assert.AreEqual(catalog.GetItemEntry(position.Item), position);
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Price, position.Price);
            }
        }
    }
}