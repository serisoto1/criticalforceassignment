using System.Collections.Generic;
using CriticalForce.Catalog;
using CriticalForce.DiContainer;
using CriticalForce.Item.Drink;
using CriticalForce.Item.Food;
using CriticalForce.Item.Weapon;
using NUnit.Framework;
using SimpleInjector;

namespace CriticalForce.Tests
{
    /// <summary>
    /// Tests for [VendingMachine]
    /// Test naming convention:
    /// {method name}_{scenario}_{expected behaviour}
    /// </summary>
    [TestFixture]
    public class VendingMachineTests
    {
        private Container _container;
        private TestSetProvider _testSetProvider;

        [SetUp]
        public void Setup()
        {
            var containerProvider = new BasicConfigurationDiContainerProvider();
            _container = containerProvider.Provide();
            _testSetProvider = new TestSetProvider();
        }

        // Create empty Vending Machine and fill it with some items
        [Test]
        public void RefillProducts_VendingMachineEmpty_CatalogPositionsAdded()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            
            // Act
            vendingMachine.RefillProducts(catalog);

            // Assert
            foreach (var position in vendingMachine.Catalog.Positions)
            {
                Assert.AreEqual(catalog.GetItemEntry(position.Item), position);
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Price, position.Price);
                Assert.AreEqual(catalog.GetItemEntry(position.Item).Amount, position.Amount);
            }
        }
        
        // Add more of existing items to the Vending Machine
        [Test]
        public void RefillProducts_SameCatalogPositions_CatalogPositionsUpdated()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog1 = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            vendingMachine.RefillProducts(catalog1);
            var catalog2 = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            catalog1.UpdatePositions(catalog1.Positions);
            
            // Act
            vendingMachine.RefillProducts(catalog2);
            
            // Assert
            foreach (var position in vendingMachine.Catalog.Positions)
            {
                Assert.AreEqual(catalog1.GetItemEntry(position.Item), position);
                Assert.AreEqual(catalog1.GetItemEntry(position.Item).Price, position.Price);
            }
        }
        
        // Add new items to the Vending Machine
        [Test]
        public void RefillProducts_DifferentCatalogPositions_CatalogPositionsUpdated()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog1 = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            vendingMachine.RefillProducts(catalog1);
            var catalog2 = new VendingMachineCatalog(_testSetProvider.GetWeaponPositions());
            catalog1.UpdatePositions(catalog2.Positions);
            
            // Act
            vendingMachine.RefillProducts(catalog2);
            
            // Assert
            Assert.AreEqual(catalog1, vendingMachine.Catalog);
        }
        
        // Add Items over the capacity limit
        [Test]
        public void RefillProducts_ProductAmountGreaterThanCapacity_CatalogUpdatedWithoutOverflow()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog1 = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            vendingMachine.RefillProducts(catalog1);
            var catalog2 = new VendingMachineCatalog(_testSetProvider.GetWeaponPositions());
            while (catalog1.GetTotalCapacity() < vendingMachine.Configuration.Capacity)
            {
                catalog1.UpdatePositions(catalog2.Positions);
            }
            
            // Act
            vendingMachine.RefillProducts(catalog1);
            
            // Assert
            Assert.LessOrEqual(vendingMachine.Catalog.GetTotalCapacity(), vendingMachine.Configuration.Capacity);
            foreach (var position in vendingMachine.Catalog.Positions)
            {
                Assert.AreEqual(catalog1.GetItemEntry(position.Item), position);
                Assert.AreEqual(catalog1.GetItemEntry(position.Item).Price, position.Price);
            }
        }
        
        // Try to buy item with enough money
        [Test]
        public void BuyItem_MoneyGreaterThanPrice_ReturnsItemAndChange()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            
            vendingMachine.RefillProducts(catalog);
            
            var desiredPosition = vendingMachine.Catalog.Positions[0];
            var myMoney = desiredPosition.Price + 999;
            
            // Act
            var result = vendingMachine.BuyItem(desiredPosition.Item, myMoney);
            
            // Assert
            Assert.AreEqual(desiredPosition.Item, result.PurchasedItem);
            Assert.AreEqual(myMoney - desiredPosition.Price, result.MoneyChange);
        }
        
        // Try to buy item with less money than the price
        [Test]
        public void BuyItem_MoneyLessThanPrice_ReturnsNoItemAndAllMoney()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            
            vendingMachine.RefillProducts(catalog);
            
            var desiredPosition = vendingMachine.Catalog.Positions[0];
            var myMoney = desiredPosition.Price / 2;
            
            // Act
            var result = vendingMachine.BuyItem(desiredPosition.Item, myMoney);
            
            // Assert
            Assert.AreEqual(null, result.PurchasedItem);
            Assert.AreEqual(myMoney, result.MoneyChange);
        }

        // Try to buy item that is out of stock
        [Test]
        public void BuyItem_ItemAmountIsZero_ReturnsNoItemAndAllMoney()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            vendingMachine.RefillProducts(catalog);

            var desiredPosition = vendingMachine.Catalog.Positions[0];
            desiredPosition.Amount = 0;
            
            var myMoney = desiredPosition.Price + 999;
            
            // Act
            var result = vendingMachine.BuyItem(desiredPosition.Item, myMoney);
            
            // Assert
            Assert.AreEqual(null, result.PurchasedItem);
            Assert.AreEqual(myMoney, result.MoneyChange);
        }

        // Try to buy item that is not in the Catalog
        [Test]
        public void BuyItem_ItemNotInCatalog_ReturnsNoItemAndAllMoney()
        {
            // Arrange
            var vendingMachine = _container.GetInstance<VendingMachine>();
            var catalog = new VendingMachineCatalog(_testSetProvider.GetDiversePositions());
            vendingMachine.RefillProducts(catalog);

            var desiredItem = new Food("Exotic Crab", "Crab with lemon sauce");
            
            var myMoney = 999;
            
            // Act
            var result = vendingMachine.BuyItem(desiredItem, myMoney);
            
            // Assert
            Assert.AreEqual(null, result.PurchasedItem);
            Assert.AreEqual(myMoney, result.MoneyChange);
        }
    }
}