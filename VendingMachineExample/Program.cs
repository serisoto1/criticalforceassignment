﻿using System;
using System.Collections.Generic;
using CriticalForce;
using CriticalForce.Catalog;
using CriticalForce.DiContainer;
using CriticalForce.Item.Drink;
using CriticalForce.Item.Food;
using CriticalForce.Item.Weapon;

namespace VendingMachineExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create new Vending Machine with Basic Configuration
            var container = new BasicConfigurationDiContainerProvider().Provide();
            var vendingMachine = container.GetInstance<VendingMachine>();
            Console.WriteLine(vendingMachine.Catalog);
            
            // Prepare Items for sale
            var item1 = new Weapon("AK-47", 30, 30);
            var item2 = new Food("CritBurger", "The most explosive burger you'll ever try!");
            var item3 = new Drink("CritCola", 1);
            
            // Decide on prices and amount of products
            var position1 = new CatalogPosition(item1, 299.00, 1);
            var position2 = new CatalogPosition(item2, 10.50, 10);
            var position3 = new CatalogPosition(item3, 5.20, 15);
            var positions = new List<CatalogPosition>{position1, position2, position3};

            // Add prepared products to the Vending machine
            vendingMachine.RefillProducts(positions);

            // Go shopping
            var myMoney = 20.00;
            var result = vendingMachine.BuyItem(item1, myMoney);
            myMoney = result.MoneyChange;
            
            result = vendingMachine.BuyItem(item2, myMoney);
            myMoney = result.MoneyChange;
            Console.WriteLine($"Money after purchase: {myMoney:C2}");
        }
    }
}