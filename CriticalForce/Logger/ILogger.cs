﻿namespace CriticalForce.Logger
{
    /// <summary>
    /// Basic Logger interface
    /// </summary>
    public interface ILogger
    {
        void Info(string message);
        void Debug(string message);
        void Warning(string message);
        void Error(string message);
    }
}