﻿using System;

namespace CriticalForce.Logger
{
    /// <summary>
    /// Logger with basic output to Console
    /// </summary>
    public class ConsoleLogger: ILogger
    {
        public void Info(string message)
        {
            Console.WriteLine($"[INFO] {message}");
        }

        public void Debug(string message)
        {
            Console.WriteLine($"[DEBUG] {message}");
        }

        public void Warning(string message)
        {
            Console.WriteLine($"[WARNING] {message}");
        }

        public void Error(string message)
        {
            Console.WriteLine($"[ERROR] {message}");
        }
    }
}