﻿using System.IO;

namespace CriticalForce.Logger
{
    /// <summary>
    /// Logger with output to local "CriticalLogs.log" file
    /// </summary>
    public class FileLogger: ILogger
    {
        private const string FileName = "CriticalLogs.log";

        public void Info(string message)
        {
            File.AppendAllText(FileName, $"[INFO] {message}\r\n");
        }

        public void Debug(string message)
        {
            File.AppendAllText(FileName, $"[DEBUG] {message}\r\n");
        }

        public void Warning(string message)
        {
            File.AppendAllText(FileName, $"[WARNING] {message}\r\n");
        }

        public void Error(string message)
        {
            File.AppendAllText(FileName, $"[ERROR] {message}\r\n");
        }
    }
}