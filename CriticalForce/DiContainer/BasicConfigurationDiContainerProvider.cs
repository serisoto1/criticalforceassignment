﻿using CriticalForce.CatalogPositionCapacityFilter;
using CriticalForce.Logger;
using CriticalForce.Models;
using SimpleInjector;

namespace CriticalForce.DiContainer
{
    /// <summary>
    /// Container providing [VendingMachine]s injected with basic configuration:
    /// Filter:           [DiscardingCatalogPositionCapacityFilter]
    /// Configuration:    ["BasicVendingMachine", capacity 50]
    /// Logger:           [ConsoleLogger]
    /// </summary>
    public class BasicConfigurationDiContainerProvider: IDiContainerProvider
    {
        public Container Provide()
        {
            var container = new Container();
            
            container.Register<ILogger, ConsoleLogger>(Lifestyle.Singleton);
            container.Register<ICatalogPositionCapacityFilter, DiscardingCatalogPositionCapacityFilter>(Lifestyle.Singleton);
            container.Register<VendingMachine>();
            container.RegisterInstance(new VendingMachineConfiguration("BasicVendingMachine", 50));
            
            container.Verify();
            
            return container;
        }
    }
}