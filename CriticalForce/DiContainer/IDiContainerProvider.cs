﻿using SimpleInjector;

namespace CriticalForce.DiContainer
{
    /// <summary>
    /// Allows us to register  and provide containers with different configurations
    /// </summary>
    public interface IDiContainerProvider
    {
        Container Provide();
    }
}