﻿using System;

namespace CriticalForce.Item
{
    /// <summary>
    /// Basic Item interface
    /// </summary>
    public interface IItem
    {
        string Name { get; }
    }
}