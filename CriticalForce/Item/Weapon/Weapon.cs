﻿using System;
using Microsoft.SqlServer.Server;

namespace CriticalForce.Item.Weapon
{
    /// <summary>
    /// Weapon item class
    /// Has [IItem] comparison implementation
    /// </summary>
    public class Weapon: IItem
    {
        public int MaxAmmo { get; }
        public int CurrentAmmo { get; set; }
        public string Name { get; }
        
        public Weapon(string name, int maxAmmo, int currentAmmo = 0)
        {
            Name = name;
            MaxAmmo = maxAmmo;
            CurrentAmmo = currentAmmo;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is Weapon))
            {
                return false;
            }
            var otherWeapon = (Weapon) obj;
            return Name == otherWeapon.Name && MaxAmmo == otherWeapon.MaxAmmo;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = (hash * 7) + Name.GetHashCode();
            hash = (hash * 7) + MaxAmmo.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return $"Weapon [Name: {Name}, Ammo: {CurrentAmmo}/{MaxAmmo}]";
        }
    }
}