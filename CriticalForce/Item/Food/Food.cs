﻿using System;
using System.Collections.Generic;

namespace CriticalForce.Item.Food
{
    /// <summary>
    /// Food item class
    /// Has [IItem] comparison implementation
    /// </summary>
    public class Food: IItem
    {
        public string Description { get; }
        public string Name { get; }
        
        public Food(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is Food))
            {
                return false;
            }
            var otherFood = (Food) obj;
            return Name == otherFood.Name && Description == otherFood.Description;
        }

        public override int GetHashCode()
        {
            int hash = 15;
            hash = (hash * 7) + Name.GetHashCode();
            hash = (hash * 7) + Description.GetHashCode();
            return hash;
        }
        
        public override string ToString()
        {
            return $"Food [Name: {Name}, Description: {Description}]";
        }
    }
}