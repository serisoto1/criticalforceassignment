﻿using System;

namespace CriticalForce.Item.Drink
{
    /// <summary>
    /// Drink item class
    /// Has [IItem] comparison implementation
    /// </summary>
    public class Drink: IItem
    {
        public string Name { get; }
        int LiterAmount { get; }

        public Drink(string name, int amount)
        {
            Name = name;
            LiterAmount = amount;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is Drink))
            {
                return false;
            }
            var otherDrink = (Drink) obj;
            return (Name == otherDrink.Name) && (LiterAmount == otherDrink.LiterAmount);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Name.GetHashCode();
            hash = (hash * 7) + LiterAmount.GetHashCode();
            return hash;
        }
        
        public override string ToString()
        {
            return $"Drink [Name: {Name}, Liters: {LiterAmount}]";
        }
    }
}