﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CriticalForce.Item;

namespace CriticalForce.Catalog
{
    /// <summary>
    /// Catalog class. Provides an interface for interaction with [CatalogPosition]s.
    /// </summary>
    public class VendingMachineCatalog
    {
        // Store positions with item hashcode as keys for easier selection
        private Dictionary<int, CatalogPosition> _positions;

        // Return a list of positions, so that hashcodes don't confuse anyone
        public List<CatalogPosition> Positions => _positions.Values.ToList();

        public VendingMachineCatalog(List<CatalogPosition> positions)
        {
            _positions = new Dictionary<int, CatalogPosition>();
            
            foreach (var position in positions)
            {
                _positions[position.Item.GetHashCode()] = new CatalogPosition(position);
            }
        }

        // Add [CatalogPosition]s from [newPositions] and update current [_positions]
        public void UpdatePositions(List<CatalogPosition> newPositions)
        {
            foreach (var position in newPositions)
            {
                if (_positions.TryGetValue(position.Item.GetHashCode(), out var existingPosition))
                {
                    existingPosition.Amount = position.Amount + existingPosition.Amount;
                    existingPosition.Price = position.Price;
                }
                else
                {
                    _positions[position.Item.GetHashCode()] = new CatalogPosition(position);
                }
            }
        }
        
        // Find [CatalogPosition] in [_positions] by its [item]
        public CatalogPosition GetItemEntry(IItem item)
        {
            _positions.TryGetValue(item.GetHashCode(), out var existingPosition);
            return existingPosition;
        }

        public int GetTotalCapacity()
        {
            var capacity = 0;
            foreach (var position in Positions)
            {
                capacity += position.Amount;
            }

            return capacity;
        }

        // Reduce amount of [item] in its respective [CatalogPosition].
        // Mostly used for buying an item
        public void DecrementItemCount(IItem item)
        {
            if (!_positions.TryGetValue(item.GetHashCode(), out var existingPosition)) return;
            if (existingPosition.Amount > 0)
            {
                existingPosition.Amount -= 1;
            }
        }
        
        public override bool Equals(Object obj)
        {
            if (!(obj is VendingMachineCatalog))
            {
                return false;
            }

            var otherCatalog = (VendingMachineCatalog) obj;
            
            if (otherCatalog.Positions.Count != Positions.Count)
            {
                return false;
            }
            
            // Check positions for equality 
            return otherCatalog.Positions.All(Positions.Contains);
        }

        public override int GetHashCode()
        {
            int hash = 19;
            foreach (var itemHash in _positions.Keys.ToList())
            {
                hash = hash + 7 + itemHash;
            }
            return hash;
        }
        
        public override string ToString()
        {
            StringBuilder stringRepresentation = new StringBuilder();
            if (Positions.Count == 0)
            {
                stringRepresentation.Append("Catalog empty");
            }
            else
            {
                stringRepresentation.Append("Item  |  Price  |  Amount\r\n");
                foreach (var position in Positions)
                {
                    stringRepresentation.AppendLine(position.ToString());
                }
            }
            
            return stringRepresentation.ToString();
        }
    }
}