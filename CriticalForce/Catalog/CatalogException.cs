﻿using System;

namespace CriticalForce.Catalog
{
    public class CatalogException: Exception
    {
        public CatalogException() : base() { }
        public CatalogException(string message) : base(message) { }
        public CatalogException(string message, Exception inner) : base(message, inner) { }
        protected CatalogException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}