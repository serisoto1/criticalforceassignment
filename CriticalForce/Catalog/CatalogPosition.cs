﻿using System;
using CriticalForce.Item;

namespace CriticalForce.Catalog
{
    /// <summary>
    /// Data class for representing a product position in the catalog
    /// </summary>
    public class CatalogPosition
    {
        private int _itemAmount;
        private double _price;
        
        public IItem Item { get; }
        public double Price
        {
            get => _price;
            set
            {
                if (value >= 0)
                {
                    _price = value;
                }
            }
        }

        public int Amount
        {
            get => _itemAmount;
            set
            {
                if (value >= 0)
                {
                    _itemAmount = value;
                }
            }
        }

        public CatalogPosition(IItem item, double price, int amount = 0)
        {
            if (price < 0)
            {
                throw new CatalogException("Price can not be set below zero!");
            }
            
            if (amount < 0)
            {
                throw new CatalogException("Product amount can not be set below zero!");
            }
            
            Item = item;
            Price = price;
            Amount = amount;
        }

        public CatalogPosition(CatalogPosition previousPosition)
        {
            Item = previousPosition.Item;
            Price = previousPosition.Price;
            Amount = previousPosition.Amount;
        }
        
        public override bool Equals(Object obj)
        {
            if (!(obj is CatalogPosition))
            {
                return false;
            }

            var otherPosition = (CatalogPosition) obj;
            
            return otherPosition.Price == Price && otherPosition.Item.Equals(Item);
        }

        public override int GetHashCode()
        {
            int hash = 11;
            hash = (hash * 7) + Price.GetHashCode();
            hash = (hash * 7) + Item.GetHashCode();
            return hash;
        }
        
        public override string ToString()
        {
            return $"{Item}  | {Price:C2}  |  {Amount}";
        }
    }
}