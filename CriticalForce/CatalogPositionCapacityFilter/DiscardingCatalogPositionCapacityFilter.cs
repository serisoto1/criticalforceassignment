﻿using System.Collections.Generic;
using CriticalForce.Catalog;
using CriticalForce.Logger;

namespace CriticalForce.CatalogPositionCapacityFilter
{
    /// <summary>
    /// Filter that discards any [CatalogPosition.Amount] over the [capacity]
    /// </summary>
    public class DiscardingCatalogPositionCapacityFilter: ICatalogPositionCapacityFilter
    {
        private readonly ILogger _logger;
        
        public DiscardingCatalogPositionCapacityFilter(ILogger logger)
        {
            _logger = logger;
        }
        
        // Filters [CatalogPosition]s depending on the provided [capacity]
        public List<CatalogPosition> FilterProducts(List<CatalogPosition> productPositions, int capacity)
        {
            var remainingCapacity = capacity;
            List<CatalogPosition> filteredPositions = new List<CatalogPosition>();
            
            foreach (var position in productPositions)
            {
                if (position.Amount <= remainingCapacity)
                {
                    remainingCapacity -= position.Amount;
                    filteredPositions.Add(new CatalogPosition(position));
                }
                else
                {
                    _logger.Warning($"Discarded {position.Amount-remainingCapacity} Items: [{position.Item}]");
                    var newPosition = new CatalogPosition(position);
                    newPosition.Amount = remainingCapacity;
                    filteredPositions.Add(new CatalogPosition(newPosition));
                    remainingCapacity = 0;
                }
            }

            return filteredPositions;
        }
    }
}