﻿using System.Collections.Generic;
using CriticalForce.Catalog;

namespace CriticalForce.CatalogPositionCapacityFilter
{
    /// <summary>
    /// Interface for catalog filters
    /// Used to fit [CatalogPosition]s into a [VendingMachine] with limited [capacity]
    /// </summary>
    public interface ICatalogPositionCapacityFilter
    {
        // Filters [CatalogPosition]s depending on the provided [capacity]
        List<CatalogPosition> FilterProducts(List<CatalogPosition> productPositions, int capacity);
    }
}