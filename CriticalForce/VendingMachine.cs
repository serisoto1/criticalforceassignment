﻿using System.Collections.Generic;
using CriticalForce.Catalog;
using CriticalForce.CatalogPositionCapacityFilter;
using CriticalForce.Item;
using CriticalForce.Logger;
using CriticalForce.Models;

namespace CriticalForce
{
    /// <summary>
    /// Main Vending Machine class
    /// Capacity and model name are passed through [VendingMachineConfiguration]
    /// Capacity is managed with [ICatalogPositionCapacityFilter]
    /// Product positions are stored in [VendingMachineCatalog]
    /// Debug logging handled with [ILogger]
    /// </summary>
    public class VendingMachine
    {
        private readonly ILogger _logger;
        private readonly ICatalogPositionCapacityFilter _catalogPositionCapacityFilter;
        
        public readonly VendingMachineConfiguration Configuration;
        public VendingMachineCatalog Catalog { get; }

        public VendingMachine(ILogger logger, ICatalogPositionCapacityFilter catalogPositionCapacityFilter,
            VendingMachineConfiguration configuration)
        {
            _logger = logger;
            Configuration = configuration;
            _catalogPositionCapacityFilter = catalogPositionCapacityFilter;
            
            var positions = new List<CatalogPosition>();
            Catalog = new VendingMachineCatalog(positions);
        }

        // Refill from [VendingMachineCatalog]
        // Product over maximum [Configuration.Capacity] is handled by [_capacityManagementStrategy]
        public void RefillProducts(VendingMachineCatalog catalog)
        {
            RefillProducts(catalog.Positions);
        }
        
        // Refill from a list of [CatalogPosition]s
        // Product over maximum [Configuration.Capacity] is handled by [_capacityManagementStrategy]
        public void RefillProducts(List<CatalogPosition> catalogPositions)
        {
            _logger.Debug($"Refilling Vending Machine...");
            
            var remainingCapacity = Configuration.Capacity - Catalog.GetTotalCapacity();
            var filteredPositions = _catalogPositionCapacityFilter.FilterProducts(catalogPositions, remainingCapacity);
            
            Catalog.UpdatePositions(filteredPositions);
            _logger.Debug($"New Catalog:\r\n{Catalog}");
        }

        // Interface for buying a single [IItem] with [money]
        // Returns [VendingMachineOutput] with [null] [PurchasedItem] if the [IItem] can not be bought
        // Returns the requested [IItem] on a successful purchase
        // Always returns excess [money] from the operation, even if the [IItem] was not purchased
        public VendingMachineOutput BuyItem(IItem order, double money)
        {
            _logger.Debug($"User trying to buy [{order}] for {money:C2}");
            
            VendingMachineOutput output;
            string outputMessage;
            var orderPosition = Catalog.GetItemEntry(order);
            if (orderPosition == null || orderPosition.Amount == 0)
            {
                outputMessage = $"Ordered item is not available! [{order}]";
                _logger.Error(outputMessage);
                output = new VendingMachineOutput(null, money, outputMessage);
                return output;
            }
            if (money < orderPosition.Price)
            {
                outputMessage = $"Not enough money provided! {orderPosition.Item.Name} costs {orderPosition.Price:C2}. Only {money:C2} received.";
                _logger.Error(outputMessage);
                output = new VendingMachineOutput(null, money, outputMessage);
                return output;
            }

            Catalog.DecrementItemCount(orderPosition.Item);
            outputMessage = $"Item {orderPosition.Item.Name} purchased for {orderPosition.Price:C2}.";
            _logger.Info(outputMessage);
            _logger.Debug($"Catalog changed:\r\n{Catalog}");
            output = new VendingMachineOutput(orderPosition.Item, money - orderPosition.Price, outputMessage);
            return output;
        }
    }
}