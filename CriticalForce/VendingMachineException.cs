﻿using System;

namespace CriticalForce
{
    public class VendingMachineException: Exception
    {
        public VendingMachineException() : base() { }
        public VendingMachineException(string message) : base(message) { }
        public VendingMachineException(string message, Exception inner) : base(message, inner) { }
        protected VendingMachineException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}