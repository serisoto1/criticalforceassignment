﻿using CriticalForce.Item;

namespace CriticalForce.Models
{
    /// <summary>
    /// Data class holding results of a [VendingMachine] operation 
    /// </summary>
    public class VendingMachineOutput
    {
        public IItem PurchasedItem { get; }
        public double MoneyChange { get; }
        
        public string Message { get; }

        public VendingMachineOutput(IItem item, double change, string message)
        {
            PurchasedItem = item;
            MoneyChange = change;
            Message = message;
        }
        
        public override string ToString()
        {
            return Message;
        }
    }
}