﻿namespace CriticalForce.Models
{
    /// <summary>
    /// Data class storing configuration details for a [VendingMachine]
    /// </summary>
    public class VendingMachineConfiguration
    {
        public string Model { get; }
        public int Capacity { get; }

        public VendingMachineConfiguration(string model, int capacity)
        {
            Model = model;
            Capacity = capacity;
        }
    }
}